package u02



object BTrees extends App {

  // A custom and generic binary tree of elements of type A
  sealed trait Tree[A]
  object Tree {

    case class Leaf[A](value: A) extends Tree[A]

    case class Branch[A](left: Tree[A], right: Tree[A]) extends Tree[A]

    def size[A](t: Tree[A]): Int = {
      val countLeaf: A=> Int = _ => 1
      traverse(t, sum, countLeaf)
    }

    def find[A](t: Tree[A], elem: A): Boolean = {
      def compareNumber: (Int, Int) => Int = _.max(_)
      traverse(t,compareNumber,isEqualNumber(elem)).equals(1)
    }


    def count[A](t: Tree[A], elem: A): Int ={
        traverse(t,sum,isEqualNumber(elem))
    }

    private def traverse[A,B](t: Tree[A], functionCompare: (B,B) => B ,functionTree: A => B): B = t match {
      case Branch(l: Tree[A],r:Tree[A]) => functionCompare(traverse(l,functionCompare,functionTree),traverse(r,functionCompare,functionTree))
      case Leaf(e:A) => functionTree(e)
    }

    private def sum: (Int, Int)=> Int = _+_

    private def bool2int(b:Boolean): Int = b match {
      case true => 1
      case _ => 0
    }

    private def isEqualNumber[A](elem: A): A => Int = par => bool2int(elem==par)


  }

  import u02.BTrees.Tree._
  val tree = Branch(Branch(Leaf(1),Leaf(2)),Leaf(1))
  println(tree, size(tree)) // ..,3
  println( find(tree, 1)) // true
  println( find(tree, 4)) // false
  println( count(tree, 1)) // 2
}

package u02

import java.util.function.Predicate

object Optionals extends App {

  sealed trait Option[A] // An Optional data type
  object Option {
    case class None[A]() extends Option[A]
    case class Some[A](a: A) extends Option[A]

    def isEmpty[A](opt: Option[A]): Boolean = opt match {
      case None() => true
      case _ => false
    }

    def getOrElse[A, B >: A](opt: Option[A], orElse: B): B = opt match {
      case Some(a) => a
      case _ => orElse
    }

    def flatMap[A,B](opt: Option[A])(f:A => Option[B]): Option[B] = opt match {
      case Some(a) => f(a)
      case _ => None()
    }

    def filter[A](value: Option[A])(function: A => Boolean):Option[A] = value match {
      case Some(a) if function(a) => Some(a)
      case _ => None()
    }

    def map[A](value: Option[A])(pred: A => Boolean):Option[Boolean] = value match {
      case Some(a) if pred(a) => Some(true)
      case _ => None()
    }

    def map2[A](value1: Option[A])(value2: Option[A])(apply: (A,A)=>Option[A]):Option[A] = (value1,value2) match {
      case (Some(a),Some(b)) => apply(a,b)
      case _ => None()
    }

  }
  
}

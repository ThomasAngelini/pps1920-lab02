package exercise

object Parity{


  // 3a
  val parity: (Int) => String = {
    case n if (n%2) == 0 => "even"
    case _ => "odd"
  }

  def parityMethod(n: Int): String = n % 2 match {
    case 0 => "even"
    case _ => "odd"
  }


}

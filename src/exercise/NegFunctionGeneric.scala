package exercise

object NegFunctionGeneric{

  def negGeneric[A](a: (A => Boolean)): (A => Boolean) = !a(_)

  val emptyInt: Int => Boolean = _==0 // predicate on int
  val notEmptyNeg = negGeneric(emptyInt) // which type of notEmpty?

}

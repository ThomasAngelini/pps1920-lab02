package exercise


object NegFunction{

  val neg: (String => Boolean) => (String => Boolean) = x => !x(_)
  def negMethod(f: (String => Boolean)): (String => Boolean) = !f(_)

  val empty: String => Boolean = _=="" // predicate on strings
  val notEmpty = neg(empty) // which type of notEmpty?

  val notEmptyMethod = negMethod(empty) // which type of notEmpty?


}

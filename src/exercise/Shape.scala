package exercise

sealed trait Shape
  object Shape{

  case class Rectangle(width:Double,height:Double) extends Shape
  case class Square(side:Double) extends Shape
  case class Circle(radious:Double) extends Shape

  def perimeter(shape: Shape):Double = shape match {
    case Rectangle(width,height) => (2*width) + (2*height)
    case Square(side) => 4*side
    case Circle(radious) => 2*Math.PI*radious
  }

  def area(shape: Shape):Double = shape match {
    case Rectangle(width,height) => width*height
    case Square(side) => side * side
    case Circle(radious) => radious*radious*Math.PI

  }
}

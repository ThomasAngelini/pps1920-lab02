package exercise

object Compose {

  def compose[A](f: A => A, g: A => A): A => A =(par => f(g(par)))

}

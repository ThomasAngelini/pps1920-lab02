package exercise

import scala.annotation.tailrec

object Fibonacci{

  def fibTailRec(n: Int): Int = {
    @annotation.tailrec
    def fibTail(n: Int,n2: Int,counter: Int): Int = n match {
      case 0  => counter
      case _ => fibTail(n-1,n2+counter,n2)
    }
    fibTail(n,1,0)
  }

  def fib(n: Int): Int= n match {
    case 0 => 0
    case 1 => 1
    case _ => fib(n-1) + fib(n-2)
  }

}

package u02

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import exercise.Shape._

class ShapeTest {

  val shapeRectangle = Rectangle(5,10)
  val shapeCircle = Circle(5)
  val shapeSquare = Square(8)


  @Test def perimeterRectangleTest(): Unit ={
    assertEquals(30,perimeter(shapeRectangle))
  }

  @Test def perimeterCircleTest(): Unit ={
    assertEquals(5*Math.PI*2,perimeter(shapeCircle))
  }

  @Test def perimeterSquareTest(): Unit ={
    assertEquals(8*4,perimeter(shapeSquare))
  }

  @Test def areaRectangleTest(): Unit ={
    assertEquals(5*10,area(shapeRectangle))
  }

  @Test def areaCircleTest(): Unit ={
    assertEquals(5*Math.PI*5,area(shapeCircle))
  }

  @Test def areaSquareTest(): Unit ={
    assertEquals(8*8,area(shapeSquare))
  }

}

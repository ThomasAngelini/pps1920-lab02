package u02

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import u02.Optionals.Option._

class OptionTest {

  val fMap2: (Int,Int)=> Optionals.Option[Int] = (a,b)=> Some(2)

  @Test def filterTestTrue(): Unit ={
    assertEquals(Some(5),filter[Int](Some(5))(_>2))
  }

  @Test def filterTestFalse(): Unit ={
    assertEquals(None(),filter(Some(5))(_ > 8))
  }

  @Test def mapTestTrue(): Unit ={
    assertEquals(Some(true),map[Int](Some(5))(_>2))
  }

  @Test def mapTestFalse(): Unit ={
    assertEquals(None(),map(Some(5))(_ > 8))
  }

  @Test def map2TestNone1(): Unit ={
    assertEquals(None(),map2[Int](Some(5))(None())(fMap2))
  }

  @Test def map2TestNone2(): Unit ={
    assertEquals(None(),map2[Int](Some(5))(None())(fMap2))
  }

  @Test def map2TestSome(): Unit ={
    assertEquals(Some(2),map2[Int](Some(5))(Some(2))(fMap2))
  }

}

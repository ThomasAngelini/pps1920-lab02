package exercise

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test

class ParityTest{
  import Parity._

  @Test
  def TestEven(): Unit ={
    assertEquals("even",parity(2))
  }

  @Test
  def TestEvenMethod(): Unit ={
    assertEquals("even",parityMethod(2))
  }

  @Test
  def TestOdd(): Unit ={
    assertEquals("odd",parity(3))
  }

  @Test
  def TestOddMethod(): Unit ={
    assertEquals("odd",parityMethod(3))
  }

}

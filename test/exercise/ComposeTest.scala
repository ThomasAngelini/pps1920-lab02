package exercise

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test

class ComposeTest {

  import Compose._

  @Test
  def testCompose(){
    assertEquals(9,compose[Int](_-1,_*2)(5))
  }

}

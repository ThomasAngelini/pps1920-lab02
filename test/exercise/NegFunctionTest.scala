package exercise

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test

class NegFunctionTest {

  import NegFunction._

  @Test
  def notEmptyFalseTest(): Unit ={
    assertFalse(notEmpty(""))
  }

  @Test
  def notEmptyTrue(): Unit ={
    assertTrue(notEmpty("foo"))
  }

  @Test
  def notEmptyCompleteTest(): Unit ={
    assertTrue(notEmpty("foo") && !notEmpty(""))
  }

  @Test
  def notEmptyMethodFalseTest(): Unit ={
    assertFalse(notEmptyMethod(""))
  }

  @Test
  def notEmptyMethodTrue(): Unit ={
    assertTrue(notEmptyMethod("foo"))
  }

  @Test
  def notEmptyMethodCompleteTest(): Unit ={
    assertTrue(notEmptyMethod("foo") && !notEmptyMethod(""))
  }

}

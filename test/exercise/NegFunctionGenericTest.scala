package exercise

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test

class NegFunctionGenericTest {

  import NegFunctionGeneric._

  @Test
  def notEmptyFalseTest(): Unit ={
    assertFalse(notEmptyNeg(0))
  }

  @Test
  def notEmptyTrue(): Unit ={
    assertTrue(notEmptyNeg(2))
  }

  @Test
  def notEmptyCompleteTest(): Unit ={
    assertTrue(notEmptyNeg(2) && !notEmptyNeg(0))
  }
}

package exercise

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test

class PredicateTest{
  import Predicate._

  @Test
  def TestP1True(): Unit = {
    assertTrue(p1(1)(2)(3));
  }

  @Test
  def TestP1False(): Unit = {
    assertFalse(p1(3)(2)(3));
  }

  @Test
  def TestP2True(): Unit = {
    assertTrue(p2(1,2,3));
  }

  @Test
  def TestP2False(): Unit = {
    assertFalse(p2(3,2,3));
  }

  @Test
  def TestP3True(): Unit = {
    assertTrue(p3(1)(2)(3));
  }

  @Test
  def TestP3False(): Unit = {
    assertFalse(p3(3)(2)(3));
  }

  @Test
  def TestP4True(): Unit = {
    assertTrue(p4(1,2,3));
  }

  @Test
  def TestP4False(): Unit = {
    assertFalse(p4(3,2,3));
  }


}

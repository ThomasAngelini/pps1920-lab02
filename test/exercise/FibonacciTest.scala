package exercise

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test

class FibonacciTest{
  import Fibonacci._

  @Test
  def TestZero(): Unit ={
    assertEquals(0,fib(0))
  }

  @Test
  def TestTailRecZero(): Unit ={
    assertEquals(0,fibTailRec(0))
  }

  @Test
  def TestOne(): Unit ={
    assertEquals(1,fib(1))
  }

  @Test
  def TestTailRecOne(): Unit ={
    assertEquals(1,fibTailRec(1))
  }

  @Test
  def TestNumber(): Unit ={
    assertEquals(5,fib(5))
  }

  @Test
  def TestTailRecNumber(): Unit ={
    assertEquals(5,fibTailRec(5))
  }




}
